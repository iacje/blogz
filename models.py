from app import db
from datetime import datetime
import bcrypt

class Blog(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(120))
    body = db.Column(db.String(1000))
    pub_date = db.Column(db.DateTime)
    image = db.Column(db.String(1000))
    owner_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __init__(self, title, body, owner, image_url=None, pub_date=None):
        self.title = title
        self.body = body
        self.owner = owner
        self.image = image_url
        #http://flask-sqlalchemy.pocoo.org/2.1/quickstart/#simple-relationships
        if pub_date is None:
            pub_date = datetime.utcnow()
        self.pub_date = pub_date


class User(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(120), unique=True)
    pw_hash = db.Column(db.String(500))
    blogs = db.relationship('Blog', backref='owner')

    def __init__(self, username, password):
        self.username = username
        self.pw_hash = bcrypt.hashpw(str.encode(password), bcrypt.gensalt()).decode('utf-8')
