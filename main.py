from flask import request, redirect, render_template, session, flash
import re
import bcrypt
from app import app, db
from models import User, Blog

@app.before_request
def require_login():
    allowed_routes = ['login', 'list_blogs', 'index', 'signup', 'static']
    if request.endpoint not in allowed_routes and 'username' not in session:
        return redirect('/login')


@app.route('/login', methods=['POST', 'GET'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        user = User.query.filter_by(username=username).first()
        if user:
            if bcrypt.checkpw(str.encode(password), str.encode(user.pw_hash)):
                session['username'] = username
                return redirect('/new-post')

            flash('Invalid password')
            return redirect('/login')

        flash('Invalid username')
        return redirect('/login')

    return render_template('login.html', page_title="Login")


@app.route('/logout')
def logout():
    del session['username']
    return redirect('/blog')


def is_invalid(text):
    return re.search(r"\s+", text) or re.search(r"^.{0,2}$", text) or re.search(r"^.{20,}$", text)


@app.route('/signup', methods=['POST', 'GET'])
def signup():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        verify = request.form['verify']

        username_error = ''
        password_error = ''
        verify_error = ''

        if is_invalid(username):
            username_error = "That's not a valid username"
        if is_invalid(password):
            password_error = "That's not a valid password"
        if verify == '':
            verify_error = "That's not a valid password"
        if verify != password:
            verify_error = "Passwords don't match"

        existing_user = User.query.filter_by(username=username).first()
        if not existing_user:
            if not (username_error or password_error or verify_error):
                new_user = User(username, password)
                db.session.add(new_user)
                db.session.commit()
                session['username'] = username
                return redirect('/new-post')

            return render_template('signup.html', page_title="Signup", 
                username=username,
                username_error=username_error,
                password_error=password_error,
                verify_error=verify_error)

        else:
            username_error = "A user with that username already exists"
            return render_template('signup.html', page_title="Signup", username=username, username_error=username_error)

    return render_template('signup.html', page_title="Signup")


@app.route('/')
def index():
    users = User.query.order_by(User.username.asc()).all()
    return render_template('index.html', page_title="Blog Users", users=users)


@app.route('/blog')
@app.route('/blog/pg<int:page>')
def list_blogs(page=1, per_page=5):
    current_blog_id = request.args.get('id')
    current_owner_id = request.args.get('user')
    #if GET request from clicking on blog entry's title, then go to that blog's page
    if current_blog_id is not None:
        blog = Blog.query.get(current_blog_id)

        #Find the blog posts right before and after this one by id
        #https://stackoverflow.com/questions/24597932/sqlalchemy-previous-row-and-next-row-by-id
        prev_blog = Blog.query.order_by(Blog.id.desc()).filter(Blog.id < blog.id).first()
        next_blog = Blog.query.order_by(Blog.id.asc()).filter(Blog.id > blog.id).first()

        return render_template('postpage.html', blog=blog, prev_blog=prev_blog, next_blog=next_blog)

    elif current_owner_id is not None:
        blogs = Blog.query.order_by(Blog.pub_date.desc()).filter_by(owner_id=current_owner_id).paginate(page, per_page, False)
        username = User.query.filter_by(id=current_owner_id).first().username
        return render_template('userpage.html', page_title=username+ "'s Blog Posts", blogs=blogs, owner_id=current_owner_id)

    #else it's a GET request to display main page
    #https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-ix-pagination
    blogs = Blog.query.order_by(Blog.pub_date.desc()).paginate(page, per_page, False)
    return render_template('listblogs.html', page_title="Blog Posts",
        blogs=blogs)


@app.route('/new-post', methods=['POST', 'GET'])
def newpost():
    owner = User.query.filter_by(username=session['username']).first()
    #new blog post submitted
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        image = request.form['image']
        title_error = ''
        body_error = ''

        if title == '':
            title_error = "Please fill in the title"
        if body == '':
            body_error = "Please fill in the body"

        #submission is valid
        if not (title_error or body_error):
            new_blog = Blog(title, body, owner, image)
            db.session.add(new_blog)
            db.session.commit()
            blog_id = str(new_blog.id)
            return redirect('/blog?id=' + blog_id)

        return render_template('newpost.html', page_title="Add a Blog Entry",
            title=title,
            body=body,
            image=image,
            title_error=title_error,
            body_error=body_error)
    #GET request from clicking on "add a new post" link
    return render_template('newpost.html', page_title="Add a Blog Entry")


@app.route('/delete-post', methods=['POST'])
def delete_post():
    blog_id = int(request.form['id'])
    blog = Blog.query.get(blog_id)
    db.session.delete(blog)
    db.session.commit()
    return redirect('/blog')


@app.route('/edit-post', methods=['POST', 'GET'])
def edit_post():
    #updated blog post submitted
    if request.method == 'POST':
        blog_id = int(request.form['id'])
        title = request.form['title']
        body = request.form['body']
        image = request.form['image']
        title_error = ''
        body_error = ''

        if title == '':
            title_error = "Please fill in the title"
        if body == '':
            body_error = "Please fill in the body"

        #submission is valid, update blog entry
        if not (title_error or body_error):
            blog = Blog.query.get(blog_id)
            if session['username'] == blog.owner.username:
                blog.title = title
                blog.body = body
                blog.image = image
                db.session.add(blog)
                db.session.commit()
                return redirect('/blog?id=' + str(blog_id))

            flash('You do not have permission to edit that post')
            return redirect('/')

        return render_template('editpost.html', page_title="Edit a Blog Entry",
            title=title,
            body=body,
            image=image,
            blog_id=blog_id,
            title_error=title_error,
            body_error=body_error)

    #edit button clicked
    blog_id = request.args.get('id')
    blog = Blog.query.get(blog_id)
    return render_template('editpost.html', page_title="Edit a Blog Entry",
        title=blog.title,
        body=blog.body,
        image=blog.image,
        blog_id=blog.id)


def split_semicolon(string):
    """ custom filter to split string by ';'. Also removes whitespace and strips trailing ';' """
    #https://stackoverflow.com/questions/20678004/jinja2-split-string-by-white-spaces
    #https://stackoverflow.com/questions/4071396/split-by-comma-and-strip-whitespace-in-python
    return re.sub(r'\s', '', string).rstrip(";").split(';')

app.jinja_env.filters['split_semicolon'] = split_semicolon

if __name__ == '__main__':
    app.run()
